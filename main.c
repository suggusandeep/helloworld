/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>

int main()
{
    int n, i, count = 0;
    printf("Enter a Number:",n);
    scanf("%d",&n);
    for(i = 1;i <= n;i++)
    {
     if(i%2==0)
     {
     count++;
     }
    }
    if(count == 2)
    {
        printf("Prime Number");
    }
    else
    {
        printf("Not a Prime Number");
    }
    return 0;
}
